const app = getApp();
// 引入 用来发送请求的方法，路径要补全
import {request} from "../../request/index.js";
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 关键字
    content:'',
    // 求购列表
    seekList:[],
    // 求购信息展开
    remarkIcon:'iconxiala',
    currentRemark:0,
    currentFlag:false,
    // 加载到的页码（切换分类/区域时，复位）
    page:1,
    // 每页记录数
    pageSize:3,
    // 是否还有更多的数据
    hasMore:true,
    // 举报信息表单框
    showModal:false,
    // 举报的 求购 id
    informSeekId:0
  },

// 获取求购列表
  getSeekList(page,pageSize,content){
    var that = this;
    wx.request({
      url: util.BASE_URL +  'seek/search',
      method: 'GET',
      data:{
        page,
        pageSize,
        content
      },
      success:function(res){
      // 如果返回的列表为空
        if(res.data.length == 0){
          that.setData({
            seekList:[],
            hasMore:false
          });
        } else{
          that.setData({
            seekList:res.data,
            hasMore:true
          });
          if(res.data.length < that.data.pageSize){
            that.setData({
              hasMore:false
            });
          }
        }
      },
      fail:function(err){
      console.log(err);
      }
    })
  },
// 在此页面继续查询
  handleSearchSeek:function(e){
    let content = e.detail.value;
    this.setData({
      hasMore:true,
      content
    });
    this.getSeekList(1,this.data.pageSize,content);
  },
// 求购区：点击备注框，显示所有备注信息
  activeRemark:function(options){
    var that = this;
    let id = options.currentTarget.dataset.id;
    let flag = options.currentTarget.dataset.flag;

    if(flag === false){
      that.setData({
        currentRemark: id,
        currentFlag: true,
        remarkIcon: 'iconup_x'
      })
    } else if(flag === true){
      that.setData({
        currentFlag: false,
        remarkIcon:'iconxiala'
      })
    }
  },
// 求购区：联系买家
  showMessage:function(options){
    let that = this;
    let userInfo = wx.getStorageSync('userInfo');
    if(userInfo == '' || userInfo == undefined){
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration: 2000
      })
    } else{
      let user = options.currentTarget.dataset.user;
      let contactText = 'QQ: ' + user.qqNum  + '微信：' + user.wechatNum;
    console.log(user);
      wx.showModal({
        title:'联系求购者',
        content: contactText,
        confirmText:'复制',
        success:function(res){
          if(res.confirm){
            util.copyText(contactText);
          } else{
          }
        }
      })
    }
  },
// 求购区：举报
  // 打开弹出层
  handleInform:function(e){
    // 当前用户Id
    let userId = wx.getStorageSync('userInfo').id;
    if(userId != undefined){
      // 举报的求购 id
      let seekId = e.currentTarget.dataset.seekid;
    console.log(seekId);
      this.setData({
        showModal:true,
        informSeekId:seekId
      });
    } else{
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration: 2000
      })
    }
  },
// 关闭弹出层
  handleCloseModal:function(){
    this.setData({
      showModal:false
    });
  },
// 发送举报信息
handleSubmitInform:function(e){
  let that = this;
  let content = e.detail.value.informText;
  if(content == ''){
    wx.showToast({
      title: '举报内容不能为空',
      icon: 'none',
      duration: 2000
    }) 
  } else{
    // 举报人
    let userId = wx.getStorageSync('userInfo').id;
    // 举报的商品
    let seekId = this.data.informSeekId;
    wx.request({
      url: util.BASE_URL +  'inform/seekInform',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      }, 
      data:{
        userId,
        seekId,
        content
      },
      success:function(res){
        if(res.data.success == true){
          that.handleCloseModal();
          wx.showToast({
            title: '已发送，待审核',
            icon: 'none',
            duration:2000
          })
        } else{
          wx.showToast({
            title: '发送失败',
            icon: 'none',
            duration:2000
          })
        }
      },
      fail:function(err){
        wx.showToast({
          title: '网络错误',
          icon: 'none',
          duration: 2000
        })
      }
    })
  }
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let content = options.content;
    this.setData({
      content
    });
    this.getSeekList(1,this.data.pageSize,content);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    if(this.data.hasMore){
      // 求购区上拉      
      // 当前页码
      let page = this.data.page + 1;
  console.log("当前页码" + page);
      // 每页记录数
      let pageSize = this.data.pageSize;
      let content = this.data.content;
      wx.request({
        url: util.BASE_URL +  'seek/search',
        method: 'GET',
        data:{
          page,
          pageSize,
          content
        },
        success:function(res){
          if(res.data.length > 0){
            let oldSeekList = that.data.seekList;
            let concatList = res.data;
            that.setData({
              page:page,
              seekList:oldSeekList.concat(concatList),
            });
          } else{
            that.setData({
              hasMore:false
            });
          }
        },
        fail:function(err){
        console.log(err);
        }
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})