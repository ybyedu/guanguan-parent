const app = getApp();
// 引入 用来发送请求的方法，路径要补全
import {request} from "../../request/index.js";
import tool from "../../utils/tool.js";
var util = require('../../utils/util');

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 自定义 tabbar
    tabbar:{},
    // 轮播图数据
    swiperList:[],
// // 置顶图标显示
// upShow:false,
// // 是否将分类栏定位在顶部
// isScrollTop:false,
    // 导航栏数据
    categoryList:[],
    // 导航栏选中项下标
    currentNavIndex:1,
    // 导航栏滚动条的位置
    scrollLeft:0,
    // 商品列表
    commodityList:[],
    // 求购列表
    seekList:[],
    // 求购信息展开
    remarkIcon:'iconxiala',
    currentRemark:0,
    currentFlag:false,
    // 当前区域
    currentArea:'commodity',
    // 加载到的页码（切换分类/区域时，复位）
    page:1,
    // 每页记录数
    pageSize:10,
    // 是否还有更多的数据
    hasMore:true,
    // 举报信息表单框
    showModal:false,
    // 举报的 求购 id
    informSeekId:0,
    // 离开头部
    leaveTop: false,
    //显示顶部分类
    fixedNav: false
  },


  ganwukongHandle(){
    wx.showToast({
      title: '开发中,敬请期待',
    })
  },

  // 回到顶部
  goTopAction:function(e){
    if(wx.pageScrollTo){
      wx.pageScrollTo({
        scrollTop:0
      })
    } else{
      wx.showModal({
        title:"提示",
        content:"当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试"
      })
    }
  },
  // 监听窗口滑动：显示置顶图标、固定分类导航栏
  // onPageScroll: function(e){
  //   let scrollTop = e[0].scrollTop;
  //   // 置顶图标显示
  //   let isShowTop = scrollTop > 300;
  //   if(this.data.upShow == isShowTop){
  //   } else{
  //     this.setData({
  //       upShow:isShowTop
  //     });
  //   }
  //   // 分类栏置顶定位
  //   let isScrollTop = scrollTop > 230;
  //   if(this.data.isScrollTop == isScrollTop){
  //     return false;
  //   } else{
  //     this.setData({
  //       isScrollTop:isScrollTop
  //     });
  //   }

  // },

// 商品区域初始
  handleCommodityArea:function(){
    this.setData({
      page:1,
      currentArea:'commodity',
      currentNavIndex:1
    });
    // 获取商品列表
    this.getCommodityList(1,this.data.pageSize,1);
  },
// 求购区域初始
  handleSeekArea:function(){
   
  },
// 获取轮播图数据
  getSwiperList(){
    request({url:util.BASE_URL + "commodity/swiperCommodity"})
    .then(result => {
      let swiperList = result.data;
      this.setData({
        swiperList
      })
    });
  },

// 获取分类导航数据
  getCategoryList(){
    request({url:util.BASE_URL + "category/getAllCategory"})
    .then(result => {
      this.setData({
        categoryList:result.data
      })
    });
  },

// 导航栏选中项样式并居中显示，并按分类查询商品列表
  activeNav(options){
    let categoryId = options.currentTarget.dataset.id
    let offsetLeft = options.currentTarget.offsetLeft
    this.setData({
      page:1,
      currentNavIndex:categoryId,
      scrollLeft: offsetLeft - this.data.scrollViewWidth/2
    });
    // 按分类查询商品列表（id == 1 乃是全部商品）
    this.getCommodityList(1,this.data.pageSize,categoryId);
  },

// 分页获取商品列表。默认为首页、下拉刷新页面（全部类别、10条记录）
  getCommodityList(page,pageSize,categoryId){
    var that = this;
    wx.request({
      url: util.BASE_URL +  'commodity/pageOfCommodity',
      method: 'GET',
      data:{
        page,
        pageSize,
        categoryId
      },
      success:function(res){
      // 如果返回的列表为空
        if(res.data.length == 0){
          that.setData({
            commodityList:[],
            hasMore:false
          });
        } else{
          that.setData({
            commodityList:res.data,
            hasMore:true
          });
        }
      },
      fail:function(err){
      console.log(err);
      }
    })
  },


// 求购区：点击备注框，显示所有备注信息
  activeRemark:function(options){
    var that = this;
    let id = options.currentTarget.dataset.id;
    let flag = options.currentTarget.dataset.flag;

    if(flag === false){
      that.setData({
        currentRemark: id,
        currentFlag: true,
        remarkIcon: 'iconup_x'
      })
    } else if(flag === true){
      that.setData({
        currentFlag: false,
        remarkIcon:'iconxiala'
      })
    }
  },

// 求购区：联系买家
  showMessage:function(options){
    let that = this;
    let userInfo = wx.getStorageSync('userInfo');
    if(userInfo == '' || userInfo == undefined){
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration: 2000
      })
    } else{
      let user = options.currentTarget.dataset.user;
      let contactText = 'QQ: ' + user.qqNum  + '微信：' + user.wechatNum;
      wx.showModal({
        title:'联系求购者',
        content: contactText,
        confirmText:'复制',
        success:function(res){
          if(res.confirm){
            util.copyText(contactText);
          } else{
          }
        }
      })
    }
  },

// 求购区：举报
  // 打开弹出层
  handleInform:function(e){
    // 当前用户Id
    let userId = wx.getStorageSync('userInfo').id;
    if(userId != undefined){
      // 举报的求购 id
      let seekId = e.currentTarget.dataset.seekid;
      this.setData({
        showModal:true,
        informSeekId:seekId
      });
    } else{
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration: 2000
      })
    }
  },
// 关闭弹出层
  handleCloseModal:function(){
    this.setData({
      showModal:false
    });
  },
// 发送举报信息
  handleSubmitInform:function(e){
    let that = this;
    let content = e.detail.value.informText;
    if(content == ''){
      wx.showToast({
        title: '举报内容不能为空',
        icon: 'none',
        duration: 2000
      }) 
    } else{
      // 举报人
      let userId = wx.getStorageSync('userInfo').id;
      // 举报的商品
      let seekId = this.data.informSeekId;
      wx.request({
        url: util.BASE_URL +  'inform/seekInform',
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        }, 
        data:{
          userId,
          seekId,
          content
        },
        success:function(res){
          if(res.data.success == true){
            that.handleCloseModal();
            wx.showToast({
              title: '已发送，待审核',
              icon: 'none',
              duration:2000
            })
          } else{
            wx.showToast({
              title: '发送失败',
              icon: 'none',
              duration:2000
            })
          }
        },
        fail:function(err){
          wx.showToast({
            title: '网络错误',
            icon: 'none',
            duration: 2000
          })
        }
      })
    }
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 编辑自定义 tabbar
    app.editTabbar();
    // 获取轮播图数据
    this.getSwiperList();
    // 获取导航栏数据
    this.getCategoryList();
    // 获取商品列表
    this.getCommodityList(1,this.data.pageSize,1);
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 隐藏小程序默认tabbar
    app.hidetabbar();
    wx.createSelectorQuery().select('.scroll-view').boundingClientRect((rect)=>{
      this.data.scrollViewWidth = Math.round(rect.width)
     }).exec()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 隐藏小程序默认tabbar
    app.hidetabbar();
    // wx.setStorageSync('userInfo', {id: 16,
    //   openid: 'o2W6Y4gOtdbRV564NvJYViROr3qI'});
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  onPageScroll: function(e){
    const leaveTop = e.scrollTop >= 255;
    this.setData({
      leaveTop
    });
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let that = this;
    this.onLoad(1);
    wx.stopPullDownRefresh({
      success: (res) => {
        that.setData({
          page:1,
          hasMore:true,
          currentNavIndex:1,
          currentArea:'commodity'
        });
      },
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    let currentArea = this.data.currentArea;
// 商品区上拉
    if(currentArea == 'commodity' && this.data.hasMore){ 
      // 当前页码
      let page = this.data.page + 1;
    console.log("page" + page);
      // 商品分类 id
      let categoryId = this.data.currentNavIndex;
      // 每页记录数
      let pageSize = this.data.pageSize;
      wx.request({
        url: util.BASE_URL +  'commodity/pageOfCommodity',
        method: 'GET',
        data:{
          page,
          pageSize,
          categoryId
        },
        success:function(res){
          // 原来的商品列表
            let commodityList = that.data.commodityList;
          // 新查询的分页商品列表
            let concatList = res.data;
          if(res.data.length > 0){
            that.setData({
              page:page,
              commodityList: commodityList.concat(concatList)
            });
          } else{
            that.setData({
              hasMore: false
            });
          }
        },
        fail:function(err){
        }
      })
    } else if(currentArea == 'seek'  && this.data.hasMore){
// 求购区上拉      
      // 当前页码
      let page = this.data.page + 1;
      // 每页记录数
      let pageSize = this.data.pageSize;
      wx.request({
        url: util.BASE_URL +  'seek/pageOfSeek',
        method: 'GET',
        data:{
          page,
          pageSize
        },
        success:function(res){
          if(res.data.length > 0){
            let oldSeekList = that.data.seekList;
            let concatList = res.data;
            that.setData({
              page:page,
              seekList:oldSeekList.concat(concatList),
            });
          } else{
            that.setData({
              hasMore:false
            });
          }
        },
        fail:function(err){
        console.log(err);
        }
      })
    }

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})