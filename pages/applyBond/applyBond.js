// pages/applyBond/applyBond.js
import {request} from "../../request/index.js";
var util = require("../../utils/util");


Page({

  /**
   * 页面的初始数据
   */
  data: {
    commodity: {},
    swiperList: [],
    transType: 0,
    address: null,
    remark: ''
  },


  /**
   * 提交表单
   */
  submitBondHandle: function(){
    const getType = this.data.transType;
    const { name,
      id,
      phone,
      detail,
      province,
      city,
      region} = this.data.address || {};
      const applicant = wx.getStorageSync('userInfo').id;
      const commodity = this.data.commodity.id;
      const userId = this.data.commodity.user.id;
      const remark = this.data.remark;
     if(getType == 0 && this.data.address == null){
       this.verifyForm("收货地址");
       return false;
     }
     request({
      url:util.BASE_URL + `apply`,
      method: "POST",
      data: {
        applicant,
        commodity,
        userId,
        name,
        phone,
        detail,
        province,
        city,
        region,
        remark,
        getType,
        addressId: id
      }
    }).then(result => {
       console.log('返回结果', result);
       if(result.statusCode == 200){
        wx.showToast({
          title: '申请成功',
          icon: 'success',
          duration: 2000,
          success: function(){
            wx.navigateBack({
             detail: 1
            });
          }
        })
       }else{
        wx.showToast({
          title: '请稍候重试',
          icon: 'none',
        })
       }
     });
  },

  verifyForm: function(title){
    util.warning(`${title} 必输项`);
  },
  
  // 获取具体商品信息
  getCommodityDetail(id){
    request({url:util.BASE_URL + `commodity/commodityDetail/${id}`})
    .then(result => {
      this.setData({
        commodity:result.data,
        swiperList:result.data.commodityImgList
      })
    });
  },


  /**
   * 配送类型
   * @param {*} e 
   */
  transPortTypeHandle: function(e){
    const type = e.target.dataset.type;
    this.setData({
      transType: type
    });
  },

  // 查询用户的默认地址
  queryDefaultAddress: function(){
    const _self = this;
    const userId = wx.getStorageSync('userInfo').id;
    request({url:util.BASE_URL + `address/default/${userId}`})
    .then(result => {
      _self.address = result.data.data;
      console.log("查询结果", _self.address);
      _self.setData({
        address: _self.address
      });
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getCommodityDetail(options.id);
    this.queryDefaultAddress();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const _self = this;
   wx.getStorage({
     key: 'address',
     success: function(data){
       console.log("返回数据", data);
       _self.address = data.data;
      _self.setData({
        address: data.data
      });
      wx.removeStorage({
        key: 'address',
      })
     }
   });
   

   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})