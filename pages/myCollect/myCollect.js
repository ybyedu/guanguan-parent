import {request} from "../../request/index.js";
var util = require("../../utils/util")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    collectList:[]
  },

  // 获取用户收藏列表
  getCollectList:function(){
    let userId = wx.getStorageSync('userInfo').id;
    if(userId != ''){
      request({url:util.BASE_URL + "user/getCollectList/" + userId})
      .then(result => {
        this.setData({
          collectList: result.data
        })
      });
    } else{
      wx.showToast({
        title: '用户信息不存在！',
        icon: 'none',
        duration: 2000
      })
    }
  },
  // 跳转到商品详情页面，或驳回
  handleToCommodityDetail:function(e){
    let id = e.currentTarget.dataset.id;
    let state = e.currentTarget.dataset.state;
console.log(id);
console.log(state);
    if(state == 3 || state == 4){
      wx.showToast({
        title: '该商品不支持查看',
        icon: 'none',
        duration: 2000
      })
      return;
    } else{
      wx.navigateTo({
        url: '../commodity/commodity?id=' + id,
      })
    }
  },
  // 删除收藏
  handleDelete:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    wx.showModal({
      title:'提示',
      content: '确定要删除这件收藏商品吗？',
      confirmColor: 'red',
      success:function(e){
        if(e.confirm){
          wx.request({
            url: util.BASE_URL +  'user/deleteCollect/' + id,
            method: 'GET',
            success:function(res){
            console.log(res);
              if(res.data.success){
                that.onLoad(e);
                wx.showToast({
                  title: res.data.message,
                  icon: 'success',
                  duration: 2000
                })
              } else{
                wx.showToast({
                  title: res.data.message,
                  icon:'none',
                  duration: 2000
                })
              }
            }
          })
        } else if(e.cancel){
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取用户收藏列表
    this.getCollectList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})