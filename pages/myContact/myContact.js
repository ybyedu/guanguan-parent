var util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  // 提交表单
  handleFormSubmit:function(e){
    let qqNum = e.detail.value.qqNum;
    let wechatNum = e.detail.value.wechatNum;
    let openid = wx.getStorageSync('userInfo').openid;

    if(qqNum == '' || wechatNum == ''){
      this.verifyForm("QQ号、微信号都");
      return;
    }

    wx.request({
      url: util.BASE_URL + 'user/updateComplete',
      method:'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      }, 
      data:{
        qqNum,
        wechatNum,
        openid
      },
      success:function(res){
        if(res.data.success){
          let userInfo = res.data.user;
            wx.setStorageSync('userInfo', userInfo);
            wx.navigateBack({
              delta: 1
            });
            wx.showToast({
              title: '添加成功！',
              icon: 'success',
              duration: 2000
            })

        } else{
          wx.showToast({
            title: '联系方式添加失败！',
            icon: 'none',
            duration: 2000
          })
        }

      },
      fail:function(err){
        wx.showToast({
          title: '网络错误！',
          icon: 'none',
          duration:2000
        })
      }
    })
  },

  // 校验表单，并给出提示信息
  verifyForm:function(content){
    wx.showToast({
      title: content +　'不能为空！',
      icon: 'none',
      duration:2000
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})