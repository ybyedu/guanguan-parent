const app = getApp();
import {request} from "../../request/index.js";
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 自定义 tabbar
    tabbar:{},
    list: [],
    page: 1,
    limit: 3
  },

  queryHistoryByPage(){
    let {page, limit, list} = this.data;
    let params = {
      page: page,
      limit: limit
    }
    console.log("查询条件", params);
    request({url:util.BASE_URL + "history/queryByPage",
    data: params})
    .then(({data}) => {
      console.log('查询结果', data.data);
      let _list = data.data || [];
      console.log('查询结果列表:', _list);
      // const total = data.data.count;
      list.push(..._list);
      this.setData({
        list
      });
    });
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 编辑自定义 tabbar
    app.editTabbar();
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 隐藏小程序默认tabbar
    app.hidetabbar();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 隐藏小程序默认tabbar
    app.hidetabbar();
    this.setData({
      page: 1,
      limit: 3,
      list: []
    });
    this.queryHistoryByPage();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
    let {page} = this.data;

    this.setData({page: page + 1});
    this.queryHistoryByPage();

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})