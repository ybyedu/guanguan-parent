// 引入 用来发送请求的方法
import {request} from "../../request/index.js";
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbar:['全部','待结缘','已结缘', "已拒绝"],
    status: {
      0: "待结缘",
      1: "已结缘",
      2: "已拒绝"
    },
    currentBarIndex: -1,
    applyBondList:[]
  },



  // 获取该用户发布的商品列表
  getApplyBondByUserId(){
    let userId = wx.getStorageSync('userInfo').id;
    let params = {}
    this.setData({
      applyBondList: []
    })
    if(this.data.currentBarIndex != -1){
      params['status'] = this.data.currentBarIndex;
    }
    if(userId != ''){
      params['applicant'] = userId;
      console.log("查询条件", params);
      request({url:util.BASE_URL + "apply/list/",
      data: params})
      .then(({data}) => {
        console.log('查询结果', data.data);
        this.setData({
          applyBondList: data.data.list
        })
      });
    } else{
      wx.showToast({
        title: '用户信息不存在！',
        icon: 'none',
        duration: 2000
      })
    }
  },



  // 选项卡选中
  activeNav(options){
    console.log(options.currentTarget.dataset.index);
    this.setData({
      currentBarIndex:options.currentTarget.dataset.index - 1
    })
    this.getApplyBondByUserId();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     this.getApplyBondByUserId();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})