
import {request} from "../../request/index.js";
var util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    commodityList:[],
    userInfo:{}
  },
  // 获取该用户发布的售出中的商品
  getUserPublish:function(userId){
    request({url:util.BASE_URL + "user/getUserPublish/" + userId})
    .then(result => {
      this.setData({
        commodityList: result.data,
        userInfo:result.data[0].user
      })
    });
  },
    // 显示提示信息
    showMessage(options){
      let qq = this.data.userInfo.qqNum;
      let wechat = this.data.userInfo.wechatNum;
      console.log(options);
      let contactText = 'QQ: ' + qq  + '微信：' + wechat;
      wx.showModal({
        title: '联系卖家',
        content: contactText,
        confirmText:'复制',
        success:function(res){
          if(res.confirm){
            util.copyText(contactText);
          } else{
          }
        }
      })
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userId = options.userId;
console.log("用户id===");
console.log(userId);
    this.getUserPublish(userId);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})