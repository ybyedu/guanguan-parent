const app = getApp();
// 引入 用来发送请求的方法，路径要补全
import {request} from "../../request/index.js";
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 关键字
    content:"",
    // 商品列表
    commodityList:[],
    // 加载到的页码（切换分类/区域时，复位）
    page:1,
    // 每页记录数
    pageSize:4,
    // 是否还有更多的数据
    hasMore:true
  },
// 关键字查询到的出售中的商品
  getCommodityList(page,pageSize,content){
    var that = this;
    wx.request({
      url: util.BASE_URL +  'commodity/search',
      method: 'GET',
      data:{
        page,
        pageSize,
        content
      },
      success:function(res){
      // 如果返回的列表为空
        if(res.data.length == 0){
          that.setData({
            commodityList:[],
            hasMore:false
          });
        } else{
          that.setData({
            commodityList:res.data,
            hasMore:true
          });
          if(res.data.length < that.data.pageSize){
            that.setData({
              hasMore:false
            });
          }
        }
      },
      fail:function(err){
      console.log(err);
      }
    })
  },
  // 在当前页面再次搜索
  handleSearchCommodity:function(e){
    let content = e.detail.value;
    this.setData({
      hasMore:true,
      content
    });
    this.getCommodityList(1,this.data.pageSize,content);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    let content = options.content;
    this.setData({
      content
    });
    // 关键字查询到的出售中的商品
    this.getCommodityList(1,this.data.pageSize,content);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    if(this.data.hasMore){
      // 当前页码
      let page = this.data.page + 1;
    console.log("page" + page);
      // 关键字
      let content = this.data.content;
      // 每页记录数
      let pageSize = this.data.pageSize;
      wx.request({
        url: util.BASE_URL +  'commodity/search',
        method: 'GET',
        data:{
          page,
          pageSize,
          content
        },
        success:function(res){
            let commodityList = that.data.commodityList;
            let concatList = res.data;
          if(res.data.length > 0){
            that.setData({
              page:page,
              commodityList: commodityList.concat(concatList)
            });
          } else{
            that.setData({
              hasMore: false
            });
          }
        },
        fail:function(err){
        console.log(err);
        }
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})