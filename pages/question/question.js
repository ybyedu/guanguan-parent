// 引入 用来发送请求的方法，路径要补全
var util = require('../../utils/util');


Page({

  /**
   * 页面的初始数据
   */
  data: {
    value: "",
    count: 0
  },


  // 文本框输入事件
  handleInput(e){
    let {value} = e.detail;
    let count = value.length;
    this.setData({value, count});
  },

  // 提交按钮事件
  handleSubmit(){
    let {value} = this.data;
    if(value.trim() == ''){
      util.warning('信息不能为空');
      return;
    }
    let userId = wx.getStorageSync('userInfo').id;
    if(userId){
      wx.request({
        url: `${util.BASE_URL}proposal/`,
        method: 'POST',
        data: {
          userId,
          content: value
        },
        success(res){
          wx.showToast({
            title: '提交成功',
            icon: 'success',
            duration: 1500
          })
          wx.navigateBack({
            delta: 1
          })
        }
      })
    }else{
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration: 1500
      });
      wx.navigateTo({
        url: '/pages/login/login',
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})