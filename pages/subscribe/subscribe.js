// 引入 用来发送请求的方法，路径要补全
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 最多选择多少张图片
    maxFileCount:3,
    // 选好的跑腿信息图片临时路径
    imgList:[],
    // 返回服务器图片的存放位置
    imgPathList:[],
    // 已经保存到数据库中跑腿信息的 id
    subscribeId:0,
    // 是否显示添加图片按钮（最多添加五张图片）
    showChooseView:true,
    index:0
  },

  // 选择图片
  handleChooseImg:function(){
    var that = this;
    let maxFileCount = this.data.maxFileCount;
    // 最多选择3张图片
    if(this.data.imgList.length < maxFileCount){
      wx.chooseImage({
        count: maxFileCount,
        sizeType:['compressed'],
        sourceType:['album','camera'],
        success(res){
    console.log(res);
          let tempFiles = res.tempFiles;
          let tempFilePaths = res.tempFilePaths;
          if(tempFilePaths.length == 0){
            return;
          }
          // 单个文件大于最大文件上传大小，驳回
          for(var i = 0; i < tempFiles.length; i++){
            if(tempFiles[i].size > util.maxFileSize){
              wx.showToast({
                title: '单个文件大小不能超过 5MB ！',
                icon: 'none',
                duration: 2000
              })
              return;
            }
          }
          let imgArrNow = that.data.imgList;
          imgArrNow = imgArrNow.concat(tempFilePaths);
          if(imgArrNow.length > 3){
            wx.showToast({
              title: '最多可选择3张图片',
              icon: 'none',
              duration: 2000
            })
            return;
          }
          that.setData({
            imgList:imgArrNow
          });
          that.handleShowChooseView();
        }
      })
    } else{
      wx.showToast({
        title: '最多可选择3张图片',
        icon: 'none',
        duration: 2000
      })
    }
  },
  // 删除选中图片事件
  handleDeleteImg:function(e){
    let index = e.currentTarget.dataset.id;
    let imgList = this.data.imgList;
    imgList.splice(index,1);
    this.setData({
      imgList
    });
    this.handleShowChooseView();
  },
  // 是否显示添加图片按钮框
  handleShowChooseView:function(){
    let maxFileCount = this.data.maxFileCount;
    let imgListCount = this.data.imgList.length;
    if(imgListCount >= maxFileCount){
      this.setData({
        showChooseView:false
      });
    } else{
      this.setData({
        showChooseView:true
      });
    }
  },
  // 预览图片
  handleShowBigImg:function(e){
    let index = e.currentTarget.dataset.id;
    let imgList = this.data.imgList;
    wx.previewImage({
      urls: imgList,
      current: imgList[index]
    })
  },
  // 提交表单（校验表单、上传数据、上传图片）
  handleSubmitForm:function(e){
    let that = this;
    let data = e.detail.value;
    let {goods} = data;
    let {address} = data;
    let {phone} = data;
    let {price} = data;
    let userId = wx.getStorageSync('userInfo').id;
console.log("用户id: " + userId);
    // 校验表单
    if(goods == '' || address == '' || phone == '' || price == ''){
      this.verifyForm("除图片，所有选项");
      return;
    }

    // 提交跑腿信息信息
    this.uploadData(data).then(
      resolve=>{
        // 提交跑腿信息图片
        let subscribeId = this.data.subscribeId;
        if(subscribeId == 0){
          wx.showToast({
            title: '信息信息上传失败！',
            icon:'none',
            duration:2000
          })
          return;
        }
        let imgList = that.data.imgList;
    console.log("===上传图片文件，图片列表==");
    console.log(imgList);
        // 其余图片存入 跑腿信息图片表
        if(imgList.length > 0){
          for(let i = 0 ; i < imgList.length; i++){
            that.uploadFile(imgList[i],i);
          }
        }
        wx.navigateTo({
          url: '../success2/success2',
        })
      },
      reject=>{
        wx.showToast({
          title: '信息上传失败！请联系管理员',
          icon:'none',
          duration:2000
        })
      }
    );

  },

  // 提交表单数据
  uploadData:function(data){
    var that = this;
    let {goods} = data;
    let {address} = data;
    let {phone} = data;
    let {price} = data;
    let userId = wx.getStorageSync('userInfo').id;

    return new Promise((resolve,reject)=>{
      wx.request({
        url: util.BASE_URL + "subscribe/uploadData",
        method: 'POST',    
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        }, 
        data:{
          userId,
          goods,
          address,
          phone, 
          price
        },
        success:function(res){
        console.log(res);
          if(res.data.success){
        console.log("上传数据，返回的数据===");
        console.log(res.data);
        console.log("上传数据，跑腿信息 id===");
        console.log(res.data.subscribeId);
            // 上传成功
            that.setData({
              subscribeId:res.data.subscribeId
            });
            resolve(res);
          } else{
            reject(res);
          }
        },
        fail:function(err){
      console.log(err);
          reject(err);
        }
      })
    });
  },

  // 提交图片（第一张图片，请求另外的地址，添加跑腿信息封面图）
  uploadFile:function(filePath,index){
    let url = "subscribe/uploadFile";
    let subscribeId = this.data.subscribeId;
    wx.uploadFile({
      url: util.BASE_URL + url,
      filePath: filePath,
      name: 'imgFile',
      formData:{
        "subscribeId":subscribeId
      },
      header:{
        "Content-Type":"multipart/form-data"
      },
      success:function(res){
console.log("====图片上传成功：===");
console.log(res);
      },
      fail:function(err){
console.log(err);
      }

    })
  },
  


  // 校验表单，并给出提示信息
  verifyForm:function(content){
      wx.showToast({
        title: content +　'不能为空！',
        icon: 'none',
        duration:2000
      })
  },
// 送达时间，picker
  changeDateTime1(e) {
    let dateTimeArray1 = this.data.dateTimeArray1;
    let dateTime1 = this.data.dateTime1;
    let datetime = dateTimeArray1[0][dateTime1[0]]+ '-' +dateTimeArray1[1][dateTime1[1]]+ '-' + 
    dateTimeArray1[2][dateTime1[2]]+ ' ' +dateTimeArray1[3][dateTime1[3]]+ ':' +dateTimeArray1[4][dateTime1[4]] + ':00';
console.log(datetime);
    this.setData({ 
      dateTime1: e.detail.value,
      deliveryTime:datetime
    });
  },
  changeDateTimeColumn1(e) {
    var arr = this.data.dateTime1, dateArr = this.data.dateTimeArray1;
    arr[e.detail.column] = e.detail.value;
    dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
    this.setData({ 
      dateTimeArray1: dateArr,
      dateTime1: arr
    });
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})