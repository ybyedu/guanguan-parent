import {request} from "../../request/index.js";
var util = require("../../utils/util")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    messageList:[],
    showContent:false,
  },
  // 更新消息状态已读、未读，点击事件
  handleUpdateSign:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    let state = e.currentTarget.dataset.state;
    this.updateMessageState(id,state).then(
      resolve =>{
        that.onLoad(e);
      },
      reject =>{
        wx.showToast({
          title: '操作失败！',
          icon: 'none',
          duration: 2000
        })
      }
    );

  },
  // 更改消息状态
  updateMessageState:function(id,state){
    return new Promise((resolve,reject)=>{
      wx.request({
        url: util.BASE_URL +  'user/updateMessageState',
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        }, 
        data:{
          id,
          state
        },
        success:function(res){
          if(res.data.success){
            resolve(res);
          } else{
            reject();
          }
        },
        fail:function(err){
          reject();
        }
      })
    });
  },
  // 删除消息
  handleDeleteMsg:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    let state = e.currentTarget.dataset.state;
    wx.showModal({
      title:"提示",
      content: "删除该条信息 ?",
      confirmColor: "red",
      success:function(e){
        if(e.confirm){
          that.updateMessageState(id,state).then(
            resolve =>{
              that.onLoad(e);
            },
            reject =>{
              wx.showToast({
                title: '操作失败！',
                icon: 'none',
                duration: 2000
              })
            }
          );
        } else if(e.cancel){
        }
      }
    })
  },

  // 获取消息列表
  getMessageList:function(){
    let userId = wx.getStorageSync('userInfo').id;
    if(userId != ''){
      request({url:util.BASE_URL + "user/getMessageList/" + userId})
      .then(result => {
        this.setData({
          messageList: result.data
        })
      });
    } else{
      wx.showToast({
        title: '用户信息不存在！',
        icon: 'none',
        duration: 2000
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取消息列表
    this.getMessageList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})