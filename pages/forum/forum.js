import {request} from "../../request/index.js";
var util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 当前导航栏的位置：全部话题
    currentNavIndex:0,
    // 帖子列表
    topicList:[],
    // 加载到的页码（切换分类/区域时，复位）
    page:1,
    // 每页记录数
    pageSize:5,
    // 是否还有更多的数据
    hasMore:true,
  },
  // 导航栏选择
  handleNavActive:function(e){
    // 选中的导航栏下标（作为是否为热点的查询条件：0或1）
    let navIndex = e.currentTarget.dataset.navindex
    this.setData({
      page:1,
      currentNavIndex:navIndex
    });
    this.getTopicList(1,this.data.pageSize,navIndex);
  },
// 分页获取商品列表。默认为首页、下拉刷新页面（全部类别、10条记录）
  getTopicList(page,pageSize,hotDegree){
    var that = this;
    wx.request({
      url: util.BASE_URL +  'topic/pageOfTopic',
      method: 'GET',
      data:{
        page,
        pageSize,
        hotDegree
      },
      success:function(res){
      // 如果返回的列表为空
        if(res.data.length == 0){
          that.setData({
            topicList:[],
            hasMore:false
          });
        } else{
          that.setData({
            topicList:res.data,
            hasMore:true
          });
        }
      },
      fail:function(err){
      console.log(err);
      }
    })
  },

// 回到顶部
  goTopAction:function(e){
    if(wx.pageScrollTo){
      wx.pageScrollTo({
        scrollTop:0
      })
    } else{
      wx.showModal({
        title:"提示",
        content:"当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试"
      })
    }
  },
// 跳转到帖子发布页面（前提是先登录）
  handleToPubTopic:function(e){
    let userId = wx.getStorageSync('userInfo').id;
    if(userId == undefined){
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration:2000
      })
    } else{
      let url = e.currentTarget.dataset.url;
      wx.navigateTo({
        url: url
      })
    }
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 加载所有帖子
    this.getTopicList(1,this.data.pageSize,0);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let that = this;
    this.onLoad(1);
    wx.stopPullDownRefresh({
      success: (res) => {
        that.setData({
          page:1,
          hasMore:true,
          currentNavIndex:0
        });
      },
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if(this.data.hasMore){
      // 当前所在区域：全部/热门
      let currentNavIndex = this.data.currentNavIndex;
      // 当前页码
      let page = this.data.page + 1;
    console.log("page：" + page);
      // 每页记录数
      let pageSize = this.data.pageSize;
      wx.request({
        url: util.BASE_URL +  'topic/pageOfTopic',
        method: 'GET',
        data:{
          page,
          pageSize,
          hotDegree:currentNavIndex
        },
        success:function(res){
          // 原来的帖子列表
            let topicList = that.data.topicList;
          // 新查询出来的分页帖子列表
            let concatList = res.data;
          if(res.data.length > 0){
            that.setData({
              page:page,
              topicList: topicList.concat(concatList)
            });
          } else{
            that.setData({
              hasMore: false
            });
          }
        },
        fail:function(err){
        }
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})