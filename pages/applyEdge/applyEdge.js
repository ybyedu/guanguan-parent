const app = getApp();
import {request} from "../../request/index.js";
var util = require('../../utils/util');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 自定义 tabbar
    tabbar:{},
    navbar:['待确认','已同意','已拒绝'],
    currentBarIndex: 0,
    applyBondList: [],
    currentPage: 1,
    limit: 4,
    total: 0,
    noMore: false,
    show: false,
    id: null
  },
  
  // 获取该用户发布的商品列表
  getApplyBondByUserId(){
    let userId = wx.getStorageSync('userInfo').id;
    let {currentPage, limit, applyBondList} = this.data;
    let params = {
      page: currentPage,
      limit: limit
    }
   
    if(this.data.currentBarIndex != -1){
      params['status'] = this.data.currentBarIndex;
    }
    if(userId != ''){
      params['userId'] = userId;
      console.log("查询条件", params);
      request({url:util.BASE_URL + "apply/list/",
      data: params})
      .then(({data}) => {
        console.log('查询结果', data.data);
        let list = data.data.list || [];
        console.log('查询结果列表:', list);
        const total = data.data.count;
        applyBondList.push(...list);
        this.setData({
          applyBondList,
          total
        });
      });
    } else{
      wx.showToast({
        title: '用户信息不存在！',
        icon: 'none',
        duration: 2000
      })
    }
  },

    // 同意请求
    agreeHandle(event){
      console.log('数据', event.currentTarget.dataset.value);

      const id = event.currentTarget.dataset.value;

      request({url:util.BASE_URL + `apply/status/${id}?status=1`,
                method: 'PUT'
              }).then(({data}) => {
                console.log('更改结果', data);
                util.success();
                this.resetData();
                this.getApplyBondByUserId();
              });
    },
    // 拒绝请求
    refuseHandle(event){
      console.log('数据', event.currentTarget.dataset.value);
      const id = event.currentTarget.dataset.value;
      this.setData({show: true, id});
    },

    bindsureHandle(params){
      this.setData({show: false});
      console.log("确定信息", params);
      const text = params.detail.text;
      const id = this.data.id;
      request({url:util.BASE_URL + 
               `apply/status/${id}?status=2&refuse=${text}`,
                method: 'PUT'
              }).then(({data}) => {
                console.log('更改结果', data);
                util.success();
                this.setData({id: null});
                this.resetData();
                this.getApplyBondByUserId();
              });
    },

    binddragstartHandle(event){
      console.log('滚动事件', event);
    },

    bindscrollHandle(data){
      console.log("活动时间", data.detail.scrollTop);
    },

    // 选项卡选中
    activeNav(options){
      console.log(options.currentTarget.dataset.index);
      this.setData({
        currentBarIndex:options.currentTarget.dataset.index
      });
      this.resetData();
      this.getApplyBondByUserId();
    },

    resetData(){
      this.setData({
        currentPage: 1,
        total: 0,
        applyBondList: [],
        noMore: false
      });
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 编辑自定义 tabbar
    app.editTabbar();
    // this.resetData();
    this.getApplyBondByUserId();
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
     // 隐藏小程序默认tabbar
     app.hidetabbar();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
     // 隐藏小程序默认tabbar
     app.hidetabbar();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
     let {currentPage, total, applyBondList} = this.data;
     console.log("页面触底查询下一页");
     if(total > applyBondList.length){
      this.setData({
        currentPage: currentPage + 1
      });
      this.getApplyBondByUserId();
     }else{
       this.setData({
         noMore: true
       });
     }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})