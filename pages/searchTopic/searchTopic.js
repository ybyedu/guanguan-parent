// 引入 用来发送请求的方法，路径要补全
import {request} from "../../request/index.js";
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 关键字
    content:'',
    // 求购列表
    topicList:[],
    // 加载到的页码
    page:1,
    // 每页记录数
    pageSize:10,
    // 是否还有更多的数据
    hasMore:true,
    // 是否首次进入
    isNotFirst:false
  },
// 在此页面查询
handleSearchTopic:function(e){
  let content = e.detail.value;
  this.setData({
    hasMore:true,
    content
  });
  this.getTopicList(1,this.data.pageSize,content);
},

// 获取求购列表
  getTopicList(page,pageSize,content){
    var that = this;
    wx.request({
      url: util.BASE_URL +  'topic/search',
      method: 'GET',
      data:{
        page,
        pageSize,
        content
      },
      success:function(res){
      // 如果返回的列表为空
        if(res.data.length == 0){
          that.setData({
            topicList:[],
            hasMore:false
          });
        } else{
          that.setData({
            topicList:res.data,
            hasMore:true
          });
          if(res.data.length < that.data.pageSize){
            that.setData({
              hasMore:false
            });
          }
        }
      },
      fail:function(err){
  console.log(err);
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      isNotFirst:true
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    if(this.data.hasMore){ 
      // 当前页码
      let page = this.data.page + 1;
  console.log("当前页码" + page);
      // 每页记录数
      let pageSize = this.data.pageSize;
      let content = this.data.content;
      wx.request({
        url: util.BASE_URL +  'seek/search',
        method: 'GET',
        data:{
          page,
          pageSize,
          content
        },
        success:function(res){
          if(res.data.length > 0){
            let oldTopicList = that.data.topicList;
            let concatList = res.data;
            that.setData({
              page:page,
              topicList:oldTopicList.concat(concatList),
            });
          } else{
            that.setData({
              hasMore:false
            });
          }
        },
        fail:function(err){
        console.log(err);
        }
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})