
// 引入 用来发送请求的方法
import {request} from "../../request/index.js";
var util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    seekList:[],
    remark:'remark',
    remarkIcon:'iconxiala',
    currentRemark:0,
    currentFlag:false
  },
  // 点击备注框，显示所有备注信息
  activeRemark:function(options){
    var that = this;
    let id = options.currentTarget.dataset.id;
    let flag = options.currentTarget.dataset.flag;

    if(flag === false){
      that.setData({
        currentRemark: id,
        currentFlag: true,
        remarkIcon: 'iconup_x'
      })
    } else if(flag === true){
      that.setData({
        currentFlag: false,
        remarkIcon:'iconxiala'
      })
    }
  },
  // 获取该用户的求购列表
  getSeekListByUserId(){
    let userId = wx.getStorageSync('userInfo').id;
    if(userId != ''){
      request({url:util.BASE_URL + "user/getSeekList/" + userId})
      .then(result => {
        this.setData({
          seekList: result.data
        })
      });
    } else{
      wx.showToast({
        title: '用户信息不存在！',
        icon: 'none',
        duration: 2000
      })
    }

  },
  // 删除求购信息
  handleDelete:function(e){
    let that = this;
    let id = e.currentTarget.dataset.seekid;
    wx.showModal({
      title: '提示',
      content: '确定要删除这条求购信息吗！',
      confirmColor :'red',
      success:function(e){
        if(e.confirm){
          wx.request({
            url: util.BASE_URL +  'user/deleteSeek/' + id,
            method: 'GET',
            success:function(res){
            console.log(res);
              if(res.data.success){
                that.onLoad(e);
                wx.showToast({
                  title: res.data.message,
                  icon: 'success',
                  duration: 2000
                })
              } else{
                wx.showToast({
                  title: res.data.message,
                  icon:'none',
                  duration: 2000
                })
              }
            }
          })
        } else if(e.cancel){
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取该用户的求购列表
    this.getSeekListByUserId();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})