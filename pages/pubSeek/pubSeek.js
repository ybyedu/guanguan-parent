var util = require("../../utils/util");


Page({

  /**
   * 页面的初始数据
   */
  data: {
     // 选好的帖子图片临时路径
     imgList:[],
     // 返回服务器图片的存放位置
     imgPathList:[],
     // 是否显示添加图片
    showChooseView: true
  },

  // 选择图片
  handleChooseImg:function(){
    var that = this;
    // 最多选择5张图片
    if(this.data.imgList.length < 5){
      wx.chooseImage({
        count: 5,
        sizeType:['compressed'],
        sourceType:['album','camera'],
        success(res){
          let tempFilePaths = res.tempFilePaths;
          console.log(tempFilePaths);
          if(tempFilePaths.length == 0){
            return;
          }
          let imgArrNow = that.data.imgList;
          imgArrNow = imgArrNow.concat(tempFilePaths);
          if(imgArrNow.length > 5){
            wx.showToast({
              title: '最多可选择5张图片',
              icon: 'none',
              duration: 2000
            })
            return;
          }
          that.setData({
            imgList:imgArrNow
          });
          that.handleShowChooseView();
        }
      })
    } else{
      wx.showToast({
        title: '最多可选择5张图片',
        icon: 'none',
        duration: 2000
      })
    }
  },
  // 删除选中图片事件
  handleDeleteImg:function(e){
    let index = e.currentTarget.dataset.id;
    let imgList = this.data.imgList;
    imgList.splice(index,1);
    this.setData({
      imgList
    });
    this.handleShowChooseView();
  },
  // 是否显示添加图片按钮框
  handleShowChooseView:function(){
    let imgListCount = this.data.imgList.length;
    if(imgListCount >= 5){
      this.setData({
        showChooseView:false
      });
    } else{
      this.setData({
        showChooseView:true
      });
    }
  },
  // 预览图片
  handleShowBigImg:function(e){
    let index = e.currentTarget.dataset.id;
    let imgList = this.data.imgList;
    wx.previewImage({
      urls: imgList,
      current: imgList[index]
    })
  },

  cancelHandle: function(){
    wx.showModal({
      content: '您确定要退出吗',
      cancelColor: '#333333',
      confirmColor: '#477FF7',
      success: function(res){
        if(res.confirm){
          wx.navigateBack({
            delta: 1,
          })
        }
      }
    });
  },

  // 提交求购信息
  handleSubmitForm:function(e){
    console.log(e.detail.value);
    let data = e.detail.value;
    let {title} = data;
    let {minPrice} = data;
    let {maxPrice} = data;
    let {remark} = data;
    let userId = wx.getStorageSync('userInfo').id;
    if(title == ''){
      this.verifyForm("标题");
      return;
    }
    if(minPrice == '' || maxPrice == ''){
      this.verifyForm("价格");
      return;
    }
    if(parseFloat(minPrice)  > parseFloat(maxPrice)){
      wx.showToast({
        title: '最小价格不能大于最大价格！',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if(remark == ''){
      this.verifyForm("备注");
      return;
    }
    // 提交数据
    wx.request({
      url: util.BASE_URL + 'seek/uploadSeek',
      method: 'POST',
      header:{
        "content-type":"application/x-www-form-urlencoded"
      },
      data:{
        title,
        minPrice: 0,
        maxPrice: 0,
        remark,
        userId
      },
      success:function(res){
        if(res.data.success){
          wx.navigateTo({
            url: '../success/success',
          })
        } else{
          wx.showToast({
            title: res.data.message,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },

// 校验表单
verifyForm:function(content){
  wx.showToast({
    title: content + '不能为空',
    icon: 'none',
    duration: 2000
  })
},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})