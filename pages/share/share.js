// pages/share/share.js
import {request} from "../../request/index.js";
import tool from "../../utils/tool.js";
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
      // 商品列表
      commodityList:[],
      currentRemark:0,
      currentFlag:false,
      // 当前区域
      currentArea:'commodity',
      // 加载到的页码（切换分类/区域时，复位）
      page:1,
      // 每页记录数
      pageSize:10,
      // 是否还有更多的数据
      hasMore:true,
      // 举报信息表单框
      showModal:false,
      // 举报的 求购 id
      informSeekId:0
  },

  // 分页获取商品列表。默认为首页、下拉刷新页面（全部类别、10条记录）
  getCommodityList(page,pageSize,categoryId){
    var that = this;
    wx.request({
      url: util.BASE_URL +  'commodity/pageOfCommodity',
      method: 'GET',
      data:{
        page,
        pageSize,
        categoryId
      },
      success:function(res){
      // 如果返回的列表为空
        if(res.data.length == 0){
          that.setData({
            commodityList:[],
            hasMore:false
          });
        } else{
          that.setData({
            commodityList:res.data,
            hasMore:true
          });
        }
      },
      fail:function(err){
      console.log(err);
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})