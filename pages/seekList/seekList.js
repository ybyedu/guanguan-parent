// pages/seekList/seekList.js
// 引入 用来发送请求的方法，路径要补全
import {request} from "../../request/index.js";
import tool from "../../utils/tool.js";
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    seekList: [],
    page: 1,
    pageSize: 10
  },

  // 获取求购列表
  getSeekList(page,pageSize){
    var that = this;
    wx.request({
      url: util.BASE_URL +  'seek/pageOfSeek',
      method: 'GET',
      data:{
        page,
        pageSize
      },
      success:function(res){
      // 如果返回的列表为空
        if(res.data.length == 0){
          that.setData({
            seekList:[],
            hasMore:false
          });
        } else{
          that.setData({
            seekList:res.data,
            hasMore:true
          });
        }
      },
      fail:function(err){
      console.log(err);
      }
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      page:1,
      currentArea:'seek'
    });
    // 获取求购列表
    this.getSeekList(1,this.data.pageSize);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})