// 引入 用来发送请求的方法
import {request} from "../../request/index.js";
var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbar:['我发布的','我关注的'],
    currentBarIndex:0,
    // 我发布的帖子
    topicList:[],
    // 我关注的帖子
    attentionList:[],
    page: 1,
    pageSize: 10
  },
// 选项卡选中
  activeNav(options){
    let activeBarIndex = options.currentTarget.dataset.index;
    if(activeBarIndex == 0){
      // 获取该用户关注的帖子列表
      this.getTopicByUserId();
    } else if(activeBarIndex == 1){
      // 获取该用户关注的帖子列表
      this.getAttentionList();
    }
    this.setData({
      currentBarIndex:activeBarIndex,
      page: 1
    })
  },
// 获取该用户发布的帖子列表
  getTopicByUserId(){
    let userId = wx.getStorageSync('userInfo').id;
    let {page, pageSize} = this.data;
    if(userId != ''){
      request({url:util.BASE_URL + "topic/pageOfTopic",
      data: {
        userId,
        page, pageSize,
      }})
      .then(result => {
        this.setData({
          topicList: result.data
        })
      });
    } else{
      wx.showToast({
        title: '用户信息不存在！',
        icon: 'none',
        duration: 2000
      })
    }
  },

// 获取用户关注列表
  getAttentionList:function(){
    let userId = wx.getStorageSync('userInfo').id;
    if(userId != ''){
      request({url:util.BASE_URL + "user/getAttentionList/" + userId})
      .then(result => {
        this.setData({
          attentionList: result.data
        })
      });
    } else{
      wx.showToast({
        title: '用户信息不存在！',
        icon: 'none',
        duration: 2000
      })
    }
  },

// 跳转到帖子详情页面，或驳回
handleToTopicDetail:function(e){
      let id = e.currentTarget.dataset.id;
      let state = e.currentTarget.dataset.state;
  console.log(id);
  console.log(state);
      if(state == 0){
        wx.showToast({
          title: '该帖子已被删除',
          icon: 'none',
          duration: 2000
        })
        return;
      } else if(state == 1){
        wx.navigateTo({
          url:"../topic/topic?id=" + id
        })
      }
    },

// 删除帖子
  handleDeleteRecord:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确定删除这个帖子吗 ?',
      confirmColor: 'red',
      success:function(e){
        if(e.confirm){
          wx.request({
            url: util.BASE_URL +　'user/delTopic/' + id,
            method: 'POST',
            success:function(res){
              if(res.data.success){
                // 刷新该页面的数据
                that.onShow();
                wx.showToast({
                  title: '已删除',
                  icon: 'succes',
                  duration: 2000
                })
              } else{
                wx.showToast({
                  title: '删除失败',
                  icon: 'none',
                  duration: 2000
                })
              }
            },
            fail:function(err){
              wx.showToast({
                title: '网络错误',
                icon: 'none',
                duration: 2000
              })
            }
          })
        } else if(e.cancel){
        }
      }
    })

  },

// 取消关注
  handleCancelAttention:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    wx.showModal({
      title:'提示',
      content: '确定要取消关注这个话题么？',
      success:function(e){
        if(e.confirm){
          wx.request({
            url: util.BASE_URL +  'user/deleteAttention/' + id,
            method: 'GET',
            success:function(res){
            console.log(res);
              if(res.data.success){
                // 刷新该页面的数据
                that.onShow();
                wx.showToast({
                  title: res.data.message,
                  icon: 'success',
                  duration: 2000
                })
              } else{
                wx.showToast({
                  title: res.data.message,
                  icon:'none',
                  duration: 2000
                })
              }
            }
          })
        } else if(e.cancel){
        }
      }
    })

  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取该用户发布的帖子列表
    this.getTopicByUserId();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let currentBarIndex = this.data.currentBarIndex;
    if(currentBarIndex == 0){
      this.getTopicByUserId();
    } else if(currentBarIndex == 1){
      this.getAttentionList();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
     let  page = this.data.page;
     this.setData({page: page + 1});

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})