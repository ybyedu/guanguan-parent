const app = getApp();
import {request} from "../../request/index.js";
var util = require('../../utils/util');


Page({

  /**
   * 页面的初始数据
   */
  data: {
    btnDisabled:false,
    userInfo:{}
  },

// 返回用户中心界面
  handleBack(){
    wx.navigateBack({
      delta: 1,
    })
  },

/**
 * 登录
 */
  handleLogin: function(){
    var that = this;
    this.setData({
      btnDisabled:true
    });
  // 获取用户信息，并登录
    this.getUserProfile().then(
      resolve=>{
        wx.login({
          success:function(res){
            let code = res.code;
            wx.request({
              url: util.BASE_URL + 'user/login/' + code,
              method: 'POST',
              success:function(result){
                let data = result.data;
                if(data.success){
                  let token = data.token;
                  let openid = data.user.openid;
                  // 将 token 存入缓存
                  let storageToken = wx.getStorageSync('token');
                  if(storageToken == ''){
                    wx.setStorageSync('token', token);
                  }
                  // 如果该用户信息不完整
                  if(data.haveUserInfo == "0"){
                    that.updateUserInfo(openid);
                  } else if(data.haveUserInfo === "1" || data.haveUserInfo === "2" || data.haveUserInfo === "3"){  // 信息初步、全部完整、禁用
                    let user = result.data.user;
                    wx.setStorageSync('userInfo', user);
                //  跳转到用户中心
                    wx.switchTab({
                      url: '/pages/user/user',
                    })
                    wx.showToast({
                      title: '登录成功',
                      icon:'success',
                      duration: 2000,
                      success: function(){
                        that.onShow();
                      }
                    })
                  }
                }else{
                  wx.showToast({
                    title: '登录失败',
                    icon: 'none',
                    duration: 2000
                  })
                } 
            }
            }) 
          },
          fail:function(e){
            wx.showToast({
              title: '网络错误！',
              icon: 'none',
              duration
            })
          }
        })
      },
      reject=>{
        wx.showToast({
          title: '用户信息获取失败',
          icon:'none',
          duration:2000
        })
      }
    );
  },
  /**
   * 更新用户信息one
   */
  updateUserInfo:function(openid){
    console.log(openid);
    var that = this;
    let userInfo = this.data.userInfo;
console.log(userInfo);
    wx.request({
      url: util.BASE_URL + 'user/updateOne',
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        "openid":openid,
        "nickName":userInfo.nickName,
        "avatarUrl":userInfo.avatarUrl,
        "gender":userInfo.gender
      },
      dataType:"json",
      success: function(result){
    console.log("====用户信息=====");
        let user = result.data.user;
    console.log(user);
        wx.setStorageSync('userInfo', user);
      //  跳转到用户中心
        wx.switchTab({
          url: '/pages/user/user',
        })
        wx.showToast({
          title: '登录成功',
          icon:'success',
          duration: 2000,
          success: function(){
            that.onShow();
          }
        })
      }
    })
  },

// 获取用户信息
  getUserProfile(){
    return new Promise((resolve,reject) =>{
      wx.getUserProfile({
        desc: '用于完善用户信息',
        success:(res) =>{
          this.setData({
            userInfo:res.userInfo
          })
          resolve(res);
        },
        fail(err){
          console.log(err);
          reject(err);
        }
      })
    })
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})