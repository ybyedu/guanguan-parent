const app = getApp();
import {request} from "../../request/index.js";
var util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 自定义 tabbar
    tabbar:{},
    userInfo:{},
    showContact:true,
    messageCount:0,
    canEmploy:false,
    // 第一次登陆
    firstLogin:true,
    userData: {}
  },
  /**
   * 获取用户信息
   */
  getUserInfo:function(e){
    const userInfo = e.detail.userInfo;
    wx.setStorageSync('userInfo', userInfo);
    // this.onShow();
  },
/**
 * 登录
 */
handleLogin: function(){
  var that = this;
  wx.showModal({
    title:'登录提示',
    content: '是否进行登录/注册，公布你的用户名、头像',
    confirmText:'确定登录',
    cancelText:'暂不登录',
    cancelColor:'#cccccc',
    confirmColor: '#00FF00',
    success(res){
      if (res.confirm) {
        wx.login({
          success:function(res){
            let code = res.code;
            wx.request({
              url: util.BASE_URL + 'user/login/' + code,
              method: 'POST',
              success:function(result){
                let data = result.data;
                if(data.success){
                  let token = data.token;
                  let openid = data.user.openid;
                  // 将 token 存入缓存
                  let storageToken = wx.getStorageSync('token');
                  if(storageToken == ''){
                    wx.setStorageSync('token', token);
                  }
                  // 如果该用户信息不完整
                  if(data.haveUserInfo == "0"){
                    that.updateUserInfo(openid);
                  } else if(data.haveUserInfo === "1" || data.haveUserInfo === "2" || data.haveUserInfo === "3"){  // 信息初步、全部完整、禁用
                    let user = result.data.user;
                    wx.setStorageSync('userInfo', user);
                    wx.showToast({
                      title: '登录成功',
                      icon:'success',
                      duration: 2000,
                      success: function(){
                        that.onShow();
                      }
                    })
                  }
                  // 列表功能设置可用
                  that.setData({
                    canEmploy: true
                  });
                }else{
                  wx.showToast({
                    title: '登录失败',
                    icon: 'none',
                    duration: 2000
                  })
                } 
            }
            }) 
          },
          fail:function(e){
            wx.showToast({
              title: '网络错误！',
              icon: 'none',
              duration
            })
          }
        })
      } else if (res.cancel) {
        wx.showToast({
          title: '点击头像登录/注册',
          icon:'none',
          duration: 2000
        })
      }
    }
  })


},
  /**
   * 更新用户信息one
   */
  updateUserInfo:function(openid){
    var that = this;
    wx.getUserInfo({
      success:function(res){
        wx.request({
          url: util.BASE_URL + 'user/updateOne',
          method: "POST",
          header: {
            'content-type': 'application/x-www-form-urlencoded' 
          },
          data: {
            "openid":openid,
            "nickName":res.userInfo.nickName,
            "avatarUrl":res.userInfo.avatarUrl,
            "gender":res.userInfo.gender
          },
          dataType:"json",
          success: function(result){
            console.log("====用户信息=====");
            let user = result.data.user;
            console.log(user);
            wx.setStorageSync('userInfo', user);
            wx.showToast({
              title: '登录成功',
              icon:'success',
              duration: 2000,
              success: function(){
                that.onShow();
              }
            })
          }
        })
      }
    })
  },

  // 获取用户消息数量
  getMessageCount:function(){
    let userId = wx.getStorageSync('userInfo').id;
    if(userId == undefined){
      return;
    } else{
      request({url:util.BASE_URL + "user/getMsgCount/" + userId})
      .then(result => {
        this.setData({
          messageCount:result.data
        })
      });
    }

  },

  // 点击用户功能列表，没有登录不可用
  handleCanEmploy:function(e){
    // 用户
    let userInfo = wx.getStorageSync('userInfo');
    if(userInfo == '' || userInfo == undefined){
      wx.showToast({
        title: '请先登录！',
        icon: 'none',
        duration: 2000
      })
    } else{
      let url = e.currentTarget.dataset.url;
      wx.navigateTo({
        url
      })
    }

  },

// 意见反馈消息提示框
  handleShowOpinion:function(){
    wx.showModal({
      title:'提示',
      content:'您有任何意见请发送邮件至 2436009116@qq.com',
      confirmText:'复制邮箱',
      success:function(e){
        if(e.confirm){
          util.copyText("2436009116@qq.com");
        } else{
        }
      }
    })
  },

// 分享给其他微信用户
  onShareAppMessage:function(res) {
  console.log(res);
    if (res.from == 'button') {
        console.log(res.target, res)
    }
    return {
      title:'我正在小程序==挑啊==发布二手商品，一起来瞅瞅吧',
      path:'/pages/index/index',//这里是被分享的人点击进来之后的页面
      imageUrl: '../../icons/publish-a.png'//这里是图片的路径
    }
  },


  queryUserInfos(){
    let userId = wx.getStorageSync('userInfo').id;
    let that = this;
    // 登录有查询
    if(userId){     
      wx.request({
        url: `${util.BASE_URL}user/userCenter/${userId}`,
        method: 'GET',
        success(res){
          console.log('查询返回的数据', res);
          that.setData({
            userData: res.data
           });
        },
      })
      
    }

  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 编辑自定义 tabbar
    app.editTabbar();

  },

  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 隐藏小程序默认tabbar
    app.hidetabbar();
    const userInfo = wx.getStorageSync('userInfo')
    let showContact = this.data.showContact;
    let canEmploy = this.data.canEmploy;
    if(showContact){
      if(userInfo.haveUserInfo == 2 || userInfo.haveUserInfo == 3){
        showContact = false
      }
    }
    if(userInfo != '' && userInfo != undefined){
      canEmploy = true;
    }
    this.setData({
      userInfo,
      showContact,
      canEmploy
    })
// 获取用户消息数量
    this.getMessageCount();

    this.queryUserInfos();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 隐藏小程序默认tabbar
    app.hidetabbar();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})