// pages/addressBook/addressBook.js

var util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    checkedIndex: 0,
    addressList: [],
    applyFlag: false
  },

  onDefAddressHandle: function(e){
    const id = e.target.dataset.value;
    const status = e.detail.value? 1: 0;
    const _self = this;
    let userId = wx.getStorageSync('userInfo').id;
    wx.request({
      url: util.BASE_URL + `address/update/${id}/${userId}?status=${status}`,
      method: "PUT",
      success: function(){
        _self.userAddressList();
      }
    })
  },

  /**
   * 编辑
   * @param {*} e 
   */
  editAddressHandle: function(e){
    const id = e.target.dataset.value;
    wx.navigateTo({
      url: '../addAddress/addAddress?id=' + id,
    })
  },

  /**
   * 删除
   */
  deleteAddressHandle: function(e){
    const id = e.target.dataset.value;
    const _self = this;
    wx.showModal({
      title: '提示',
      content: '确定删除此地址',
      success (res) {
        if (res.confirm) {
          _self.deleteAddress(id);
        } else if (res.cancel) {
        }
      }
    })
  },

  deleteAddress: function(id){
    const _self = this;
    wx.request({
      url: util.BASE_URL + `address/${id}`,
      method: "DELETE",
      success: function(res){
         if(res.data.success){
           util.success();
            _self.userAddressList();
         }else{
           util.error();
         }
      },
      fail: function(){
        util.error();
      }
    })
  },

  /**
   * 查询用户列表
   */
  userAddressList: function(){
    const userId = wx.getStorageSync('userInfo').id;
    const _self = this;
      wx.request({
        url: util.BASE_URL + `address/list/${userId}`,
        method: "GET",
        success: function(res){
          const data = res.data;
          if(data.success){
             _self.data.addressList = data.data;
             _self.setData({
              addressList: data.data
             });
          }else{
            util.error();
          }
        },
        fail: function(){
          util.error();
        }
      })
  },

  selectAddressHandle: function(e){
    if(!this.data.applyFlag){
      return false;
    }
    const id = e.currentTarget.dataset.value;
    let item = {};
    let {addressList} = this.data;
    for(let i = 0; i < addressList.length;i ++){
        if(id == addressList[i].id){
           item = addressList[i];
           break;
        }
    }
 
    wx.setStorage({
      key: 'address',
      data: item,
      success: function(){
        wx.navigateBack({
          detail: 1
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     this.setData({
       applyFlag: options.applyFlag
     })
     this.data.applyFlag = options.applyFlag;
     this.userAddressList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.userAddressList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})