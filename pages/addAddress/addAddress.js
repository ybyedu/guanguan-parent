// pages/addAddress/addAddress.js

var util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defChecked: false,
    id: null,
    name: '',
    phone: '',
    detail: '',
    region: [],
    detail: ''
  },

  /**
   * 点击默认地址单选框事件
   */
  onClickHandle: function(){
    console.log('点击事件');
     this.data.defChecked = !this.data.defChecked;
     this.setData({
       defChecked: this.data.defChecked
     });
  },

  /**
   * 清空按钮事件
   * @param {*} e 
   */
  cleanInfoHandle: function(e){
      this.setData({
        name: '',
        phone: '',
        region: [],
        detail: '',
        id: null,
        defChecked: false
      });
  },

  /**
   * 省市县/区 选择框事件
   * @param {*} e 
   */
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
  },

  /**
   * 提交地址信息
   * @param {*} e 
   */
  submitAddressHandle: function(e){
   if(this.data.name.trim() == ''){
       util.warning('请输入姓名');
       return;
   }
   if(this.data.phone.trim() == ''){
      util.warning('请输入电话');
      return;
    }
    if(this.data.region.length == 0){
      util.warning('请选择省市区');
      return;
    }
    if(this.data.detail.trim() == ''){
      util.warning('请输入详细地址');
      return;
    }
    
    const {id, name, phone, region, 
           detail, defChecked} = this.data;
    let userId = wx.getStorageSync('userInfo').id;
    const _self = this;
    wx.request({
      url: util.BASE_URL + 'address/addOrUpdate',
      method: 'POST', 
      data: {
        id,
        name,
        phone,
        detail,
        isDefault: defChecked? 1: 0,
        province: region[0],
        city: region[1],
        region: region[2],
        userId
      },
      success:function(){
        util.success(function(){
          wx.navigateBack({
            detail: 1
          })
        });
         _self.cleanInfoHandle();
      },
      fail: function(){
        util.error();
      }
    })
  },


  queryAddress: function(id){
    
    const _self = this;
    wx.request({
      url: util.BASE_URL + `address/${id}`,
      method: "GET",
      success: function(res){
         const data = res.data;
         if(data.success){
          const {id, name, phone, 
                  region, 
                  detail, 
                  isDefault,
                  province,
                  city} = data.data;
            _self.setData({
              id,
              name,
              phone,
              detail,
              defChecked: isDefault == 1,
              region: [province, city, region]
            });
         }
      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     const id = options.id;
     if(id){
        wx.setNavigationBarTitle({
          title: '编辑地址',
        })
        this.data.id = id;
        this.queryAddress(id);
     }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})