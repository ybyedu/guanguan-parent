// 引入 用来发送请求的方法
import {request} from "../../request/index.js";
var util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbar:['待结缘','已结缘','已下架'],
    currentBarIndex:0,
    commodityList:[],
    limit: 5,
    page: 1,
    state: 0
  },
// 选项卡选中
  activeNav(options){
    console.log(options.currentTarget.dataset.index);
    this.setData({
      currentBarIndex:options.currentTarget.dataset.index,
      page: 1
    });
    this.getCommodityByUserId();
  },

  // 获取该用户发布的商品列表
  getCommodityByUserId(){
    let userId = wx.getStorageSync('userInfo').id;
    let {page, limit, currentBarIndex} = this.data;
    if(userId != ''){
      request({url:util.BASE_URL + "commodity/myList/", 
    method: 'GET',
    data: {
      userId,
      page,
      limit,
      state: currentBarIndex + 1
    }})
      .then(result => {
        console.log("查询的结果", result);
        this.setData({
          commodityList: result.data.data.records
        })
      });
    } else{
      wx.showToast({
        title: '用户信息不存在！',
        icon: 'none',
        duration: 2000
      })
    }

  },
  // 更改商品状态，点击事件
  handleUpdateState:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    let state = e.currentTarget.dataset.state;
    let content = "";
    if(state == 2){
      content = "该商品已售罄 ?";
    } else if(state == 3){
      content = "下架该商品 ?";
    }
    // 提示框
      wx.showModal({
        title: '提示',
        content: content,
        confirmColor: 'red',
        success:function(e){
          if(e.confirm){
            that.updateState(id,state).then(
              resolve =>{
                that.onLoad(e);
                wx.showToast({
                  title: '操作成功',
                  icon: 'success',
                  duration:2000
                })
              },
              reject =>{
                wx.showToast({
                  title: '操作失败！',
                  icon: 'none',
                  duration: 2000
                })
              }
            );
          } else if(e.cancel){
          }
        }
      })
  },

  // 更改商品状态
  updateState:function(id,state){
    return new Promise((resolve,reject)=>{
      wx.request({
        url: util.BASE_URL +  'user/updateCommodityState',
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        }, 
        data:{
          id,
          state
        },
        success:function(res){
          if(res.data.success){
            resolve(res);
          } else{
            reject();
          }
        },
        fail:function(err){
          reject();
        }
      })
    });
  },
  // 售罄商品、下架商品，删除记录
  handleDeleteRecord:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确定删除该商品吗 ?',
      confirmColor: 'red',
      success:function(e){
        if(e.confirm){
          wx.request({
            url: util.BASE_URL +　'user/delCommodity/' + id,
            method: 'POST',
            success:function(res){
              if(res.data.success){
                that.onLoad(e);
                wx.showToast({
                  title: '已删除',
                  icon: 'succes',
                  duration: 2000
                })
              } else{
                wx.showToast({
                  title: '删除失败',
                  icon: 'none',
                  duration: 2000
                })
              }
            },
            fail:function(err){
              wx.showToast({
                title: '网络错误',
                icon: 'none',
                duration: 2000
              })
            }
          })
        } else if(e.cancel){
        }
      }
    })

  },
  // 点击下架商品，不支持跳转
  handleUnderCommodity:function(){
    wx.showToast({
      title: '下架商品不支持查看',
      icon: 'none',
      duration:2000
    })
  },

  // 同意或拒绝按钮事件
  agreeOrRejectHandle(e){
    let item = e.currentTarget.dataset.value;
    let status = e.currentTarget.dataset.flag;
    let index = e.currentTarget.dataset.index;
    let that = this;
    wx.request({
      url: `${util.BASE_URL}apply/status/${item.id}?status=${status}&commId=${item.commodity}`,
      method: 'PUT',
      data:{
      },
      success(result){
        console.log("操作结果", result);
        let list = result.data.data;
        console.log('返回数据',list);
        let commodityList = that.data.commodityList;
        commodityList[index].applyBonds = list;
        that.setData({commodityList})
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userId = options.userId;
    // 获取该用户发布的商品列表
    this.getCommodityByUserId();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
     let page = this.data.page;
     this.setData({page: page + 1});
     this.getCommodityByUserId();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})