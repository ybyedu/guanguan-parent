App({

  // 隐藏小程序自带的 tabBar
  hidetabbar(){
    wx.hideTabBar({
      // 如果失败了，定时重试
      fail:function(){
        setTimeout(wx.hideTabBar(),500);
      }
    })
  },
  // 获取设备信息
  getSystemInfo:function(){
    let that = this;
    wx.getSystemInfo({
      success: (result) => {
        that.globalData.systemInfo = result;
      },
    })
  },
  // tabBar
  editTabbar:function(){
    let tabbar = this.globalData.tabBar;
    let currentPages = getCurrentPages();
    let that = currentPages[currentPages.length - 1];
    // 这里获取到的是app.json配置的tabbar路径
    let route = that.route;
    let pagePath = '';
    for (let i = 0 ; i < tabbar.list.length; i++) {
      pagePath = tabbar.list[i].pagePath.substr(6);
      tabbar.list[i].selected = false;
      if(pagePath == route) { 
        tabbar.list[i].selected = true;
      }
    }
  console.log(tabbar);
    that.setData({
      tabbar: tabbar
    });
  },
  // 参数
  globalData:{
    "systemInfo":null,
    tabBar:{
      "backgroundColor": "#ffffff",
        "color": "#777777",
        "selectedColor": "#416DF2",
        "list": [
          {
            "pagePath": "../../pages/index/index",
            "text": "首页",
            "iconPath": "../../icons/tabbar/home.png",
            "selectedIconPath": "../../icons/tabbar/home-d.png"
          },
          // {
          //   "pagePath": "../../pages/applyEdge/applyEdge",
          //   "text": "结缘",
          //   "iconPath": "../../icons/tabbar/edge.png",
          //   "selectedIconPath": "../../icons/tabbar/edge-d.png"
          // },
          {
            "pagePath": "../../pages/publish/publish",
            "text": "",
            "iconPath": "../../icons/tabbar/publish.png",
            "selectedIconPath": "../../icons/tabbar/publish.png",
            "isSpecial":true
          },
          // {
          //   "pagePath": "../../pages/history/history",
          //   "text": "课堂",
          //   "iconPath": "../../icons/tabbar/classroom.png",
          //   "selectedIconPath": "../../icons/tabbar/classroom-d.png"
          // },
          {
            "pagePath": "../../pages/user/user",
            "text": "我的",
            "iconPath": "../../icons/tabbar/my.png",
            "selectedIconPath": "../../icons/tabbar/my-d.png"
          }
        ]
    }
  },

  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {
    // 隐藏小程序自带的 tabBar
    this.hidetabbar();
    // 获取设备信息
    this.getSystemInfo();
  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {
    
  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {
    
  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function (msg) {
    
  },

  onPageNotFound:function (options){
    
  }
})
