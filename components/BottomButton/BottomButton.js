Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    cancelHandle: function(){
      const that = this;
      wx.showModal({
        content: '您确定要退出吗',
        cancelColor: '#333333',
        confirmColor: '#477FF7',
        success: function(res){
          if(res.confirm){
            that.triggerEvent("cancel", {});
          }
        }
      });
    },
    submitHandle: function(){
      this.triggerEvent("submit", {});
    }
  }
})
