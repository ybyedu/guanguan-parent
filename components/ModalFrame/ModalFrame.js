// components/ModalFrame/ModalFrame.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String
    },
    isShowConfirm: {
      type: Boolean,
      default: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {
    
 
  setValue: function (e) {
    this.setData({
      walletPsd: e.detail.value
    })
  },
  cancel: function () {
    var that = this
    that.setData({
      isShowConfirm: false,
    })
  },
  confirmAcceptance:function(){
    var that = this
    console.log("输入的信息为", this.data.walletPsd);
    const text = this.data.walletPsd;
    this.triggerEvent("sure",{text});
  },
  }
})
