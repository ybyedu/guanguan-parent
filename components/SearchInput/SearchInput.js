var util = require("../../utils/util");

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    currentArea:{
      type:String,
      value:''
    },
    searchContent:{
      type:String,
      value:''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleSearch:function(e){
      let currentArea = this.properties.currentArea;
      if(currentArea == "commodity"){
        this.handleSearchCommodity(e);
      } else if(currentArea == "seek"){
        this.handleSearchSeek(e);
      }
    },
    // 搜索商品
    handleSearchCommodity:function(e){
      let content = e.detail.value;
    console.log(content);
      wx.navigateTo({
        url: '../../pages/searchCommodity/searchCommodity?content=' + content
      })
    },
    // 搜索求购
    handleSearchSeek:function(e){
      let content = e.detail.value;
    console.log(content);
      wx.navigateTo({
        url: '../../pages/searchSeek/searchSeek?content=' + content
      })
    },
  }
})
