// components/publishComp/publishPanel.js
const util = require("../../utils/util");
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    // 功能区域显示
    showFunctionArea:true,
    // 自定义 tabbar
    tabbar:{},
    publisList: [
      {
        icon: '../../icons/publish/help.png',
        text: '发求助',
        subText: '缘分总会不期而遇',
        verify: 'seek'
      },
      // {
      //   icon: '../../icons/publish/feeling.png',
      //   text: '发感悟',
      //   subText: '在平凡里照见美好',
      //   verify: 'topic'
      // },
      {
        icon: '../../icons/publish/physical.png',
        text: '发实物',
        subText: '没有买卖，只有免费分享',
        verify: 'commodity'
      }
    ],
    //-6zSNJb7SstEgC1mrmmejgQ2Vss19BdNR3wt1W7nOz0 申请
    //'mIZqIcMHi1FrYDspjIvUsCt5Yhl-Vvn_oGzn_nL7IXs',
    tempIds: ['-6zSNJb7SstEgC1mrmmejgQ2Vss19BdNR3wt1W7nOz0']
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 点击关闭按钮
    closePublishHandle: function(){
      this.triggerEvent("close", {});
    },
    

    // 验证该用户是否登录，并且填写的联系方式
    handleVerifyContact:function(options){
      let selectUrl = options.currentTarget.dataset.verify;
      let url = '';
      if(selectUrl == 'commodity'){
        url = '../pubCommodity/pubCommodity';
      } else if(selectUrl == 'seek'){
        url = '../pubSeek/pubSeek';
      } else if(selectUrl == 'topic'){
        url = '../pubTopic/pubTopic';
      }
     
      let userInfo = wx.getStorageSync('userInfo');
      if(userInfo == ''){
        wx.showToast({
          title: '请先登录！',
          icon:'none',
          duration:2000
        })
        wx.navigateTo({
          url: "../login/login"
        });
        return ;
      } else if(userInfo != null){
        let openid = userInfo.openid;
        let _slef = this;
        wx.request({
          url: util.BASE_URL + 'user/getHaveUserInfo',
          method: 'POST',
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          }, 
          data:{
            openid
          },
          success:function(result){
            if(result.data.success == true){
              userInfo = result.data.user;
              if(userInfo.haveUserInfo == 3){
                wx.showToast({
                  title: '你已经被封禁使用发布功能！',
                  icon:'none',
                  duration:2000
                });
                return;
              } else{
                _slef.setSubscribeMessage(url);
              }
            } else{
              wx.showToast({
                title: '用户权限获取失败！',
                icon:'none',
                duration:2000
              });
              return;
            }
          }
        })
      } 
    },


    // 判断是否开启了消息接收
    setSubscribeMessage: function(url){
      const tempIds = this.data.tempIds;
      const _slef = this;
      wx.getSetting({
        withSubscriptions: true,
        success: function(data){
          if(!data.subscriptionsSetting.itemSettings){
            _slef.requestmessage(url, tempIds);
          }else{
            const settings = data.subscriptionsSetting.itemSettings;
            const shouldIds = [];
            tempIds.forEach((temp, idx) => {
              if('accept' != settings[temp]){
                shouldIds.push(temp);
              }
            });
            if(shouldIds.length > 0){
              _slef.requestmessage(url, shouldIds);
            }else{
              _slef.navigateTo(url);
            }
          }
        }
      })
    },

    requestmessage: function(url, tmplIds){
      const that = this;
      wx.requestSubscribeMessage({
        tmplIds: tmplIds,
        success: function(res){
          that.navigateTo(url);
        },
        fail: function(err){
          that.navigateTo(url);
        }
      })
    },

    navigateTo: function(url){
      wx.navigateTo({
        url: url
      })
      this.closePublishHandle();
    }
  }
})
