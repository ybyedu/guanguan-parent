// tabBarComponent/tabBar.js
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 功能区显示
    showFunctionArea:{
      type: Boolean,
      value: false
    },
    // tabbar 按钮
    tabbar: {
      type: Object,
      value: {
        "backgroundColor": "#ffffff",
        "color": "#777777",
        "selectedColor": "#416DF2",
        "list": [
          {
            "pagePath": "../../pages/index/index",
            "text": "首页",
            "iconPath": "../../icons/tabbar/home.png",
            "selectedIconPath": "../../icons/tabbar/home-d.png"
          },
          // {
          //   "pagePath": "../../pages/applyEdge/applyEdge",
          //   "text": "结缘",
          //   "iconPath": "../../icons/tabbar/edge.png",
          //   "selectedIconPath": "../../icons/tabbar/edge-d.png"
          // },
          {
            "pagePath": "../../pages/publish/publish",
            "text": "",
            "iconPath": "../../icons/tabbar/publish.png",
            "selectedIconPath": "icons/tabbar/publish.png",
            "isSpecial":true
          },
          // {
          //   "pagePath": "../../pages/history/history",
          //   "text": "课堂",
          //   "iconPath": "../../icons/tabbar/classroom.png",
          //   "selectedIconPath": "../../icons/tabbar/classroom-d.png"
          // },
          {
            "pagePath": "../../pages/user/user",
            "text": "我的",
            "iconPath": "../../icons/tabbar/my.png",
            "selectedIconPath": "../../icons/tabbar/my-d.png"
          }
        ]
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isIphoneX: app.globalData.systemInfo.model.includes('iPhone X'),

    showPublish: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 显示发布功能区
    showFunctionArea:function(e){
      this.setData({showPublish: true})
      // console.log("点击事件", e);
      // const pages = getCurrentPages();
      // console.log("获取页面", pages[pages.length - 1]);
      // const currentPage = pages[pages.length - 1].route;
      // wx.setStorageSync( "backPage", currentPage);
      // let showFunctionArea = this.data.showFunctionArea;
      // if(showFunctionArea == true){
      //   this.setData({
      //     showFunctionArea:false
      //   });
      // } else{
      //   this.setData({
      //     showFunctionArea:true
      //   });
      // }
      // // 触发父组件的自定义事件，同时传递数据给父组件
      // this.triggerEvent("itemChange",{showFunctionArea});
    },

    // 关闭发布框
    closePublishPanelHandle: function(){
      this.setData({showPublish: false})
    }
  }
})
